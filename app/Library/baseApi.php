<?php

namespace App\Library;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class baseApi
{
    private $api;

    protected $endpoint;
    protected $clientOptions;
    protected $data;

    public function __construct()
    {
        $this->clientOptions = [
            'base_uri' => $this->endpoint,
            'verify' => false
        ];
    }

    protected function sendRequest($path, $params = [])
    {
        $client = new GuzzleClient($this->clientOptions);

        $data = $this->data;
        if (isset($params['data']) && is_array($params['data']))
        {
            $data = array_merge($params['data'], $data);
            unset($params['data']);
        }

        try
        {
            // send post
            if (isset($params['post']))
            {
                $response = $client->post($path, [
                    'form_params' => $data
                ]);
            }
            elseif(isset($params['put']))
            {
                $response = $client->put($path, [
                    'form_params' => $data
                ]);
            }
            else
            {
                $response = $client->get($path, [
                    'query' => $data
                ]);
            }
        }
        catch (RequestException $e)
        {
            $response = $e->getResponse();
            \Illuminate\Support\Facades\Log::warning(Psr7\str($response));
        }


        return json_decode($response->getBody(), true);
    }


}