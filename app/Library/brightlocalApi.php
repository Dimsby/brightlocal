<?php

namespace App\Library;

use App\Library\baseApi;

class brightlocalApi extends baseApi
{
    protected $endpoint = 'https://tools.brightlocal.com/seo-tools/api/';

    public function __construct()
    {
        $expires = (int) gmdate('U') + 1800;
        $this->data = [
            'api-key' => env('BRIGHTLOCAL_API_KEY'),
            'expires' => $expires,
            'sig' => base64_encode(hash_hmac('sha1', env('BRIGHTLOCAL_API_KEY').$expires, env('BRIGHTLOCAL_API_SECRET'), true))
        ];

        parent::__construct();
    }


    // batch

    public function createBatch()
    {
        $batch = $this->sendRequest('v4/batch', ['post' => true]);

        if ($batch['success']) return $batch['batch-id'];
        else return false;
    }

    public function commitBatch($batchId)
    {
        return $this->sendRequest('v4/batch', ['put' => true, 'data' => ['batch-id' => $batchId]]);
    }

    // clients

    public function getAllClients()
    {
        return $this->sendRequest('v1/clients-and-locations/clients/search');
    }

}