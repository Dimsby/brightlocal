<?php
namespace App\Http\Controllers;

use App\Library\brightlocalApi;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        die('test');
    }

    public function run()
    {
        $api = new brightlocalApi();

        $id = $api->createBatch();
        if ($id)
        {
            dump($api->commitBatch($id));
        }
    }
}